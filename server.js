const express = require('express');
const socketIO = require('socket.io');
const RiveScript = require("rivescript");

let bot = new RiveScript();

const PORT = process.env.PORT || 3000;
const INDEX = '/index.html';

const server = express()
    .use((req, res) => res.sendFile(INDEX, {root: __dirname}))
    .listen(PORT, () => console.log(`Listening on ${PORT}`));


const io = socketIO(server);

io.on('connection', (socket) => {
    console.log('Client connected');
    socket.on('disconnect', () => console.log('Client disconnected'));
});

// Load a directory full of RiveScript documents (.rive files). This is for
// Node.JS only: it doesn't work on the web!
bot.loadDirectory("brain").then(loading_done).catch(loading_error);

// Load a list of files all at once (the best alternative to loadDirectory
// for the web!)
bot.loadFile([
    "brain/eliza.rive",
]).then(loading_done).catch(loading_error);

// All file loading operations are asynchronous, so you need handlers
// to catch when they've finished. If you use loadDirectory (or loadFile
// with multiple file names), the success function is called only when ALL
// the files have finished loading.
function loading_done() {
    console.log("Bot has finished loading!");

    // Now the replies must be sorted!
    bot.sortReplies();

    // And now we're free to get a reply from the brain!

    // NOTE: the API has changed in v2.0.0 and returns a Promise now.
    bot.reply("username", "Hello, bot!").then(function (reply) {
        console.log("The bot says: " + reply);
    });
}

// It's good to catch errors too!
function loading_error(error) {
    console.log("Error when loading files: " + error);
}

io.on('connection', function (socket) {

    console.log('a user connected');
    socket.on('disconnect', function () {
        console.log('user disconected');
    });

    socket.on('chat-question', function (message) {

        bot.reply("Axel", message).then(function (reply) {
            console.log("The bot says: " + reply);
            socket.emit('chat-answer', reply);
        });
    });
});


