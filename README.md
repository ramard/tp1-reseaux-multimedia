# Chatbot
Projet d'Axel Ramard et Corentin Boulc'h

Le chatbot est basé sur le projet RiveScript. Le système de réponse est Eliza (disponible sur le [Github](https://github.com/aichaos/rivescript-js/blob/master/eg/brain/eliza.rive) de RiveScript)  

## Lancer le service en local
Il est nécessaire d'avoir NodeJS installé sur le serveur. [Plus d'informations sur le site de NodeJS](https://nodejs.org/en/) 

Se placer à la racine du projet et lancer `npm install && npm start`

## Héberger le service
Pour héberger le service il vous faut tout d'abord modifier une variable dans le fichier `index.html` (ligne 81)
```js
const URL = 'url du service';
```
C'est à partir de cette URL que le client communiquera avec le serveur via les WebSockets.

Une fois cela fait vous pouvez utiliser l'hébergeur de votre choix.

## Accéder au service en ligne
Vous pouvez retrouver notre service hébergé à cette [adresse](https://chatbot-imr3.herokuapp.com/)